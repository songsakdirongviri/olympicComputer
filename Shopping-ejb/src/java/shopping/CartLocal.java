/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopping;

import java.util.Hashtable;
import javax.ejb.Local;

/**
 *
 * @author CSTUROOM111
 */
@Local
public interface CartLocal {

    void putItem(String itemId, int quantity);

    void removeItem(String itemId);

    Hashtable<String, Integer> getItems();
    
    String getCustomerID();

}
