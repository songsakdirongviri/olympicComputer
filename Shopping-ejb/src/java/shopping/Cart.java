/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopping;

import java.util.Hashtable;
import java.util.concurrent.ThreadLocalRandom;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;

/**
 *
 * @author CSTUROOM111
 */
@Stateful
public class Cart implements CartLocal {

    private Hashtable<String, Integer> shoppingCart ;
    private String customerID ;
    
    public String getCustomerID(){
        return customerID;
    }
    @PostConstruct
    public void initialize(){
        shoppingCart = new Hashtable<String, Integer>();
        customerID =  Integer.toString(ThreadLocalRandom.current().nextInt(0, 100 + 1));
    }
    
    @Override
    public void putItem(String itemId, int quantity) {
        if(shoppingCart.containsKey(itemId) == false){
            shoppingCart.put(itemId, quantity);
        }else {
            shoppingCart.replace(itemId, shoppingCart.get(itemId).intValue() + quantity );
        }
    }

    @Override
    public void removeItem(String itemId) {
        shoppingCart.remove(itemId);
    }

    @Override
    public Hashtable<String, Integer> getItems() {
        return shoppingCart; 
    }

}
